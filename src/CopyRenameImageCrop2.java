import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;

public class CopyRenameImageCrop2 {

    public static void main(String[] argv) throws IOException {
        int storyinteger = 15;

        int startinteger = 1;

        String str_inputFolder  = "D:\\Pictures\\Mashima Hiro真岛浩\\FAIRY TAIL COMIC\\input\\" + Integer.toString(storyinteger);

        String str_outputFolder = "D:\\Pictures\\Mashima Hiro真岛浩\\FAIRY TAIL COMIC\\input\\output";

        String str_backupFolder = "D:\\Pictures\\Mashima Hiro真岛浩\\FAIRY TAIL COMIC\\input\\b" + Integer.toString(storyinteger);
    
        // convertUnicodeScriptHan(str_inputFolder);

        backupFiles( str_inputFolder,  str_backupFolder,  str_outputFolder);
      
        cropImage ( str_inputFolder,startinteger, str_outputFolder, storyinteger);
        
        File file = new File(str_inputFolder);
        delete(file);
    }
    
      public static void cropImage (String str_inputFolder, int startinteger, String str_outputFolder, int storyinteger) throws IOException{
            // ========== Get list of files from input folder =============
            File inputFolder = new File(str_inputFolder + "\\");
              File outputFolder = new File(str_outputFolder);

            // ===== count input folder files =====
            int totalfiles = countAll(inputFolder);
            System.out.println("count= " + totalfiles);

            // ====== Start modify image size ======
            Image inputimage = null;

            File[] inputlistOfFiles = inputFolder.listFiles();
            try {
                for (int i = 0; i < inputlistOfFiles.length; i++) {
                  
                   inputimage = ImageIO.read(inputlistOfFiles[i]);
                    System.out.println(""+inputimage);
                    BufferedImage bufferedInput = (BufferedImage) inputimage;
                    System.out.println(bufferedInput.getWidth());
                    System.out.println(bufferedInput.getHeight());
                 
                    final Image outputimage = bufferedInput.getSubimage(0, 110, bufferedInput.getWidth(), bufferedInput.getHeight() - 170);
                   //final Image outputimage = bufferedInput.getSubimage(0, 120, bufferedInput.getWidth(), bufferedInput.getHeight() - 120);
                  //  final Image outputimage = bufferedInput.getSubimage(0, 120, bufferedInput.getWidth(), bufferedInput.getHeight() - 170);
                    BufferedImage bufferedOutput = (BufferedImage) outputimage;

                    ImageIO.write(bufferedOutput, "jpg", inputlistOfFiles[i]);
                    System.out.println(inputlistOfFiles[i] + " new image size done");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            
              for (int i = 0; i < inputlistOfFiles.length; i++) {

                if (inputlistOfFiles[i].isFile()) {
                    String path = str_inputFolder + "\\" + inputlistOfFiles[i].getName();
                    File f = new File(path);

                    //==== rename then copy to output folder ========
                    if ((i + startinteger) <= 9) {
                        f.renameTo(new File(outputFolder + "\\" + Integer.toString(storyinteger) + "-00" + (i + startinteger) + ".jpg"));
                    } else {
                        f.renameTo(new File(outputFolder + "\\" + Integer.toString(storyinteger) + "-0" + (i + startinteger) + ".jpg"));
                    }
                }
            }
            System.out.println("Rename to output folder done");

    }
    public static void delete(File file) throws IOException {
        //========= delete the input folder ===============
      
        if (file.isDirectory()) {
            //directory is empty, then delete it
            if (file.list().length == 0) {
                file.delete();
                System.out.println("Input folder is deleted : " + file.getAbsolutePath());
            } else {

                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    delete(fileDelete);
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    System.out.println("Input folder is deleted : " + file.getAbsolutePath());
                }
            }

        } else {
            //if file, then delete it
            file.delete();
            System.out.println("File is deleted : " + file.getAbsolutePath());
        }
    }

    public static void copyFolder(File src, File dest) throws IOException {

        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
                //  System.out.println("Directory copied from "  + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile);
            }

        } else {
    		//if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes 
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            //System.out.println("File copied from " + src + " to " + dest);
        }
    }

    public static int countAll(File directory) {
        int count = 0;
        for (File file : directory.listFiles()) {
            if (file.isFile()) {
                count++;
            }
            if (file.isDirectory()) {
                count += countAll(file);
            }
        }
        return count;
    }
    
    public static void  convertUnicodeScriptHan (String str_inputFolder) {
              for (int i=0;i<str_inputFolder.length();i++){
             String str_char_at_i = Character.toString(str_inputFolder.charAt(i));
             
             int codepoint_num = str_inputFolder.codePointAt(i);
             
             if(Character.UnicodeScript.of(codepoint_num)==Character.UnicodeScript.HAN){
                 String str_decimal = Integer.toString(codepoint_num);
                 str_decimal = "&#"+str_decimal+";";
                 str_inputFolder = str_inputFolder.replace(str_char_at_i, str_decimal);
                 
             }
        }
        System.out.println(""+str_inputFolder);
    }
    
    public static void  backupFiles(String str_inputFolder, String str_backupFolder, String str_outputFolder) throws IOException{
          File outputFolder = new File(str_outputFolder);
        
        File sourceFolder = new File(str_inputFolder);

        if (!sourceFolder.exists()) {
            System.out.println(str_inputFolder + " not found.");

        } else {
            // ========= backup Folder ==========
            File backupFolder = new File(str_backupFolder);

            if (!backupFolder.exists()) {
                if (backupFolder.mkdirs()) {
                    System.out.println(str_backupFolder + " is created!");
                } else {
                    System.out.println("Failed to create ouput folder!");
                }
            }
            copyFolder(sourceFolder, backupFolder);

            // ========= Find output Folder ==========
            if (!outputFolder.exists()) {
                if (outputFolder.mkdirs()) {
                    System.out.println("Backup done! " + str_outputFolder + " is created!");
                } else {
                    System.out.println("Failed to create ouput folder!");
                }
            } 
        }
    }
    
  
}
