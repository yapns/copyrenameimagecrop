
import java.io.File;
import java.io.IOException;

public class RenameNumber {


    public static void main(String[] argv) throws IOException {

        String path= "E:\\nn\\015";
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
             
                 String str_file = listOfFiles[i].toString();
                 int extensionPos = str_file.lastIndexOf('.');
               
                 String str_num = listOfFiles[i].toString().substring(extensionPos-2,extensionPos);
                
                 String str_newname = path+"\\015-0"+ str_num+".jpg";
              //   System.out.println(listOfFiles[i]+"  rename to "+path+"\\"+ str_num+"."+getExtension(str_file));
                    System.out.println(listOfFiles[i]+"  rename to "+str_newname);
                    
                 File oldName = new File(listOfFiles[i].toString());
                 File newName = new File(str_newname);
                
                 if(oldName.renameTo(newName)) {
                     System.out.println("renamed");
                 } else {
                     System.out.println("Error");
                 }
            }
        }
    }
    
   

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int extensionPos = filename.lastIndexOf('.');
        int lastUnixPos = filename.lastIndexOf('/');
        int lastWindowsPos = filename.lastIndexOf('\\');
        int lastSeparator = Math.max(lastUnixPos, lastWindowsPos);
 
        int index = lastSeparator > extensionPos ? -1 : extensionPos;
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }
}
